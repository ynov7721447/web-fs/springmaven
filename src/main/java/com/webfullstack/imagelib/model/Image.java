package com.webfullstack.imagelib.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;


@Entity
public class Image {

    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private String path;

    // Constructors
    public Image() {
    }

    public Image(String name, String path) {
        this.name = name;
        this.path = path;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

}