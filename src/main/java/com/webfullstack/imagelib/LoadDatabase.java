package com.webfullstack.imagelib;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import com.webfullstack.imagelib.model.Image;
import com.webfullstack.imagelib.repository.ImageRepository;

@Component
public class LoadDatabase implements CommandLineRunner {

    private final ImageRepository imageRepository;

    public LoadDatabase(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        // Add initial data to the database
        Image image1 = new Image("Image 1", "/path/to/image1.jpg");
        Image image2 = new Image("Image 2", "/path/to/image2.jpg");

        // Save the images to the repository
        imageRepository.save(image1);
        imageRepository.save(image2);
    }
}
